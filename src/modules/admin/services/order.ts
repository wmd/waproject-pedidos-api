import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { isEmpty } from 'lodash';
import { IOrder } from 'modules/database/interfaces/order';
import { Order } from 'modules/database/models/order';

import { OrderRepository } from '../repositories/order';

@Injectable()
export class OrderService {
  constructor(private orderRepository: OrderRepository) {}

  public async save(model: IOrder): Promise<Order> {
    if (isEmpty(model.description)) {
      throw new BadRequestException('description-required');
    }

    if (model.quantity < 1) {
      throw new BadRequestException('invalid-quantity');
    }

    if (model.price < 0) {
      throw new BadRequestException('invalid-price');
    }

    if (model.id) return this.update(model);
    return this.create(model);
  }

  public async remove(orderId: number): Promise<void> {
    const order = await this.orderRepository.findById(orderId);

    if (!order) {
      throw new NotFoundException('not-found');
    }

    return this.orderRepository.remove(orderId);
  }

  private async create(model: IOrder): Promise<Order> {
    const order = await this.orderRepository.insert(model);

    return order;
  }

  private async update(model: IOrder): Promise<Order> {
    const order = await this.orderRepository.findById(model.id);

    if (!order) throw new NotFoundException('not-found');

    return this.orderRepository.update({ ...order, ...model });
  }
}
