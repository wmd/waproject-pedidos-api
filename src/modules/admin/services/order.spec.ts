import { BadRequestException, NotFoundException } from '@nestjs/common';

import { IOrder } from 'modules/database/interfaces/order';

import { OrderRepository } from '../repositories/order';
import { OrderService } from './order';

/* eslint-disable max-len */
describe('Admin/OrderService', () => {
  let orderRepository: OrderRepository;
  let service: OrderService;

  const order: IOrder = {
    description: 'First Product',
    quantity: 10,
    price: 2.15
  };

  beforeEach(async () => {
    orderRepository = new OrderRepository();

    service = new OrderService(orderRepository);
  });

  it('should create a order', async () => {
    jest.spyOn(orderRepository, 'insert').mockImplementationOnce(order => Promise.resolve({ ...order } as any));

    const result = await service.save(order);

    expect(result).not.toBeFalsy();
    expect(result).toEqual(order);
  });

  it('should update a order', async () => {
    jest.spyOn(orderRepository, 'findById').mockResolvedValueOnce({ id: 1 } as any);
    jest.spyOn(orderRepository, 'update').mockImplementationOnce(order => Promise.resolve({ ...order } as any));

    const result = await service.save({ id: 1, ...order });

    expect(result).not.toBeFalsy();
    expect(result).toEqual({ id: 1, ...order });
  });

  it('should throw NotFoundException when try update a not found order', async () => {
    jest.spyOn(orderRepository, 'findById').mockResolvedValueOnce(null);

    try {
      await service.save({ id: 1, ...order });
      fail();
    } catch (err) {
      expect(err).toBeInstanceOf(NotFoundException);
    }
  });

  it('should throw BadRequestException with message description-required when try save a order less description', async () => {
    jest.spyOn(orderRepository, 'insert').mockResolvedValueOnce({ description: '' } as any);

    try {
      await service.save({ ...order, description: '' });
      fail();
    } catch (err) {
      expect(err).toBeInstanceOf(BadRequestException);
      expect(err.message.message).toBe('description-required');
    }
  });

  it('should throw BadRequestException with message invalid-quantity when try save a order with negative quantity', async () => {
    jest.spyOn(orderRepository, 'insert').mockResolvedValueOnce({ quantity: -1 } as any);

    try {
      await service.save({ ...order, quantity: -1 });
      fail();
    } catch (err) {
      expect(err).toBeInstanceOf(BadRequestException);
      expect(err.message.message).toBe('invalid-quantity');
    }
  });

  it('should throw BadRequestException with message invalid-price when try save a order with negative price', async () => {
    jest.spyOn(orderRepository, 'insert').mockResolvedValueOnce({ price: -1.2 } as any);

    try {
      await service.save({ ...order, price: -1.2 });
      fail();
    } catch (err) {
      expect(err).toBeInstanceOf(BadRequestException);
      expect(err.message.message).toBe('invalid-price');
    }
  });

  it('should remove a order', async () => {
    jest.spyOn(orderRepository, 'findById').mockResolvedValueOnce({ id: 2 } as any);
    jest.spyOn(orderRepository, 'remove').mockResolvedValueOnce({ id: 2 } as any);

    await service.remove(2);
  });

  it('should throw NotFoundException when try to remove a not found order', async () => {
    jest.spyOn(orderRepository, 'findById').mockResolvedValueOnce(null);

    try {
      await service.remove(2);
      fail();
    } catch (err) {
      expect(err).toBeInstanceOf(NotFoundException);
    }
  });
});
